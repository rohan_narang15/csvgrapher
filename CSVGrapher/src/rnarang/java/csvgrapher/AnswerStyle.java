package rnarang.java.csvgrapher;

import java.util.ArrayList;

public class AnswerStyle 
{	
	public static final int RATING = 0;
	public static final int LIKELINESS = 1;
	public static final int MEAL = 2;
	public static final int FAMILIARITY = 3;
	public static final int TIME_OF_DAY = 4;
	public static final int MOTIVATION = 5;
	public static final int YES_NO = 6;
	public static final int LIKED_DISLIKED = 7;
	public static final int GENERAL_WITH_OTHER = 8;
	public static final int GENERAL = 9;
	public static final int FREQUENCY = 10;
	public static final int NUMERIC_RANGE = 11;
	public static final int CURRENCY_RANGE = 12;
	public static final int COUNT = 13;
	
	// Count style questions are of the format "How many times..." and answers are of the format "More than n times" "5" "4" etc.
	public static boolean isAnswerSetCountStyle(ArrayList<Answer> answers)
	{
		boolean result = true;
		
		for (Answer a : answers)
		{
			String answer = a.content.toLowerCase();
			
			if (answer.contains("more than") || answer.contains("less than"))
			{
				result = result && true;
			}
			else
			{
				try
				{
					Integer.parseInt(answer);
					result = result && true;
				}
				catch(NumberFormatException e)
				{
					result = result && false;
				}
				
			}			
		}
		
		return result;
	}
	
	// Rating style answers are in a range of 1-10
	public static boolean isAnswerSetRatingStyle(ArrayList<Answer> answers)
	{
		boolean result = true;
		
		for (Answer a : answers)
		{
			String answer = a.content;
			
			result = result && 
					(
						answer.startsWith("1 - ") ||
						answer.equals("2") ||
						answer.equals("3") ||
						answer.equals("4") ||
						answer.startsWith("5") ||
						answer.equals("6") ||
						answer.equals("7") ||
						answer.equals("8") ||
						answer.equals("9") ||
						answer.startsWith("10 - ")
					);
		}
		
		return result;
	}
	
	// Likeliness style answers are in range of 1-5 and might contain a likeliness string, ex: "Very Likely"
	public static boolean isAnswerSetLikelinessStyle(ArrayList<Answer> answers)
	{
		boolean result = true;
		
		for (Answer a : answers)
		{
			String answer = a.content.toLowerCase();
			
			result = result &&
					(
						answer.startsWith("1") ||
						answer.startsWith("2") ||
						answer.startsWith("3") ||
						answer.startsWith("4") ||
						answer.startsWith("5") ||
						answer.contains("likely") ||
						answer.contains("undecided")
					);
		}
		
		return result;
	}
	
	// A meal style answer contains any of the following strings: "Breakfast", "Lunch", "Snack", "Dinner", "Dessert", "After exercising"
	public static boolean isAnswerSetMealStyle(ArrayList<Answer> answers)
	{
		boolean result = true;
		
		for (Answer a : answers)
		{
			String answer = a.content.toLowerCase();
			
			result = result &&
					(
						answer.equals("breakfast") ||
						answer.equals("lunch") ||
						answer.equals("snack") ||
						answer.equals("dinner") ||
						answer.equals("dessert") ||
						answer.equals("after exercising")
					);
		}
		
		return result;
	}
	
	public static boolean isAnswerSetFamiliarityStyle(ArrayList<Answer> answers)
	{
		boolean result = true;
		
		for (Answer a : answers)
		{
			String answer = a.content;
			
			result = result && answer.toLowerCase().contains("familiar");
		}
		
		return result;
	}
	
	public static boolean isAnswerSetTimeOfDayStyle(ArrayList<Answer> answers)
	{
		boolean result = true;
		
		for (Answer a : answers)
		{
			String answer = a.content.toLowerCase();
						
			result = result &&
					(
						answer.equals("morning") ||
						answer.equals("afternoon") ||
						answer.equals("evening") ||
						answer.equals("late night")
					);
		}
		
		return result;
	}
	
	public static boolean isAnswerSetMotivationStyle(ArrayList<Answer> answers)
	{
		boolean result = false;
		
		for (Answer a : answers)
		{
			String answer = a.content.toLowerCase();
			
			result = result || answer.contains("motivated");
		}
		
		return result;
	}
	
	public static boolean isAnswerSetYesNoStyle(ArrayList<Answer> answers)
	{
		boolean foundYes = false;
		boolean foundNo = false;
		
		for (Answer a: answers)
		{
			String answer = a.content.toLowerCase();
			
			if (foundYes == false)
			{
				foundYes = answer.equals("yes") || answer.startsWith("yes");
			}
			
			if (foundNo == false)
			{
				foundNo = answer.equals("no") || answer.startsWith("no ") || answer.startsWith("no,");
			}
		}
		
		return foundYes && foundNo;
	}
	
	public static boolean isAnswerSetLikedDislikedStyle(ArrayList<Answer> answers)
	{
		boolean result = true;
		
		for (Answer a: answers)
		{
			String answer = a.content.toLowerCase();
			
			result = result &&
					(
						answer.equals("liked a lot") ||
						answer.equals("liked somewhat") ||
						answer.equals("neutral") ||
						answer.equals("disliked somewhat") ||
						answer.equals("disliked a lot")
					);
		}
		
		return result;
	}
	
	public static boolean doesAnswerSetContainOtherOption(ArrayList<Answer> answers)
	{
		for (Answer a : answers)
		{
			String answer = a.content.toLowerCase();
			if (answer.startsWith("other") || answer.startsWith("others"))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isAnswerSetFrequencyStyle(ArrayList<Answer> answers)
	{
		boolean result = true;
		
		for (Answer a : answers)
		{
			String answer = a.content.toLowerCase();
			
			result = result &&
					(
						answer.equals("sometimes") ||
						answer.equals("never") ||
						answer.equals("rarely") ||
						answer.equals("often") ||
						answer.equals("always")
					);
		}
		
		return result;
	}
		
	public static boolean isAnswerSetNumericRangeStyle(ArrayList<Answer> answers)
	{
		boolean result = true;
		
		for (Answer answer: answers)
		{
			String a = answer.content.toLowerCase();
			
			if (a.startsWith("less than") || a.startsWith("more than") || a.endsWith(" and above") || a.endsWith(" and below"))
			{
				result = result && true;
			}
			else
			{
				String[] numbericStrings = a.trim().split("-");
				try
				{
					Integer.parseInt(numbericStrings[0]);
					Integer.parseInt(numbericStrings[1]);
					
					result = result && true;
				}
				catch (Exception e)
				{
					result = result && false;
				}
			}
		}
		
		return result;
	}
	
	public static ArrayList<Range> getRangeListForNumericRangeStyleAnswerSet(ArrayList<Answer> answers)
	{
		ArrayList<Range> ranges = new ArrayList<Range>();
		
		for (Answer answer : answers)
		{
			String a = answer.content.toLowerCase();
			
			if (a.startsWith("less than"))
			{
				String num = a.replace("less than ", "");
				double ul = (double) Integer.parseInt(num);
				ranges.add(new Range(ul, Double.MIN_VALUE));
			}
			else if (a.endsWith(" and below"))
			{
				String num = a.replace(" and below", "");
				double ul = (double) Integer.parseInt(num);
				ranges.add(new Range(ul, Double.MIN_VALUE));
			}
			else if (a.startsWith("more than"))
			{
				String num = a.replace("more than ", "");
				double ll = (double) Integer.parseInt(num);
				ranges.add(new Range(Double.MAX_VALUE, ll));
			}
			else if (a.endsWith(" and above"))
			{
				String num = a.replace(" and above", "");
				double ll = (double) Integer.parseInt(num);
				ranges.add(new Range(Double.MAX_VALUE, ll));
			}
			else
			{
				String[] nums = a.trim().split("-");
				double n1 = (double)Integer.parseInt(nums[0]);
				double n2 = (double)Integer.parseInt(nums[1]);
				ranges.add(new Range(n1, n2));
			}
		}
		
		return ranges;
	}
	
	public static boolean isAnswerSetCurrencyRangeStyle(ArrayList<Answer> answers)
	{
		boolean result = true;
		
		for (Answer a : answers)
		{
			String answer = a.content.toLowerCase();
			
			result = result &&
					(
						answer.startsWith("less than $") ||
						answer.startsWith("more than $") ||
						answer.startsWith("$")
					);
		}
		
		return result;
	}
	
	public static ArrayList<Range> getRangeListForCurrencyRangeStyleAnswerSet(ArrayList<Answer> answers)
	{
		ArrayList<Range> ranges = new ArrayList<Range>();
		
		for (Answer a : answers)
		{
			String answer = a.content.toLowerCase();
			
			int startIndex = answer.indexOf("$") + 1;
			int endIndex = startIndex + 4;
			
			if (answer.startsWith("less than"))
			{
				double ul = Double.parseDouble(answer.substring(startIndex, endIndex));
				double ll = Double.MIN_VALUE;
				
				ranges.add(new Range(ul, ll));
			}
			else if (answer.startsWith("more than"))
			{
				double ll = Double.parseDouble(answer.substring(startIndex, endIndex));
				double ul = Double.MAX_VALUE;
				
				ranges.add(new Range(ul, ll));
			}
			else
			{
				double ll = Double.parseDouble(answer.substring(startIndex, endIndex));
				
				int nextStart = answer.indexOf("$", endIndex) + 1;
				int nextEnd = nextStart + 4;
				
				double ul = Double.parseDouble(answer.substring(nextStart, nextEnd));
				
				ranges.add(new Range(ul, ll));
			}
		}
		
		return ranges;
	}
	
	public static void fillInMissingAnswersForSet(ArrayList<Answer> answers)
	{
		if (answers.isEmpty())
		{
			return;
		}
		
		int style = answers.get(0).style;
		
		switch (style)
		{
			case AnswerStyle.LIKELINESS:
			{
				boolean[] valueFound = new boolean[5];
				boolean isStringBasedAnswerSet = true; 
				
				for (Answer answer : answers)
				{
					String content = answer.content;
					int index = -1;
					
					if (content.startsWith("1")) index = 0;
					else if (content.startsWith("2")) index = 1;
					else if (content.startsWith("3")) index = 2;
					else if (content.startsWith("4")) index = 3;
					else if (content.startsWith("5")) index = 4;
					
					if (index != -1)
					{
						valueFound[index] = true;
						isStringBasedAnswerSet = false;
					}
				}
				
				if (isStringBasedAnswerSet == false)
				{
					for (int i = 0; i < 5; ++i)
					{
						if (valueFound[i] == false)
						{
							Answer answer = new Answer();
							answer.content = Integer.toString(i + 1);
							answer.count = 0;
							answer.style = AnswerStyle.LIKELINESS;
							answers.add(answer);
						}
					}
				}
				
				break;
			}
		
			case AnswerStyle.RATING:
			{
				boolean[] valueFound = new boolean[10];
				
				for (int i = 0; i < 10; ++i)
				{
					valueFound[i] = false;
				}
				
				for (Answer answer : answers)
				{
					String content = answer.content;
					
					int index = 0;
					
					if (content.startsWith("10")) index = 9;
					else if (content.startsWith("1")) index = 0;
					else if (content.startsWith("5")) index = 4;
					else index = Integer.parseInt(content) - 1;
					
					valueFound[index] = true;
				}
				
				for (int i = 0; i < 10; ++i)
				{
					if (valueFound[i] == false)
					{
						Answer answer = new Answer();
						answer.content = Integer.toString(i + 1);
						answer.count = 0;
						answer.style = AnswerStyle.RATING;
						answers.add(answer);
					}
				}
				
				break;
			}
			
			default:
			{
				// Functionality unsupported for any other answer style as of now
				break;
			}
		}
	}
	
	
	public static void detectAnswerStyleForSet(ArrayList<Answer> answers)
	{
		int style;
		
		if (AnswerStyle.isAnswerSetRatingStyle(answers))
		{
			//System.out.println("Found a rating style answer set");
			style = RATING;
		}
		else if (AnswerStyle.isAnswerSetNumericRangeStyle(answers))
		{
			//System.out.println("Found a numeric range style answer set");
			style = NUMERIC_RANGE;
		}
		else if (AnswerStyle.isAnswerSetLikelinessStyle(answers))
		{
			//System.out.println("Found a likeliness style answer set");
			style = LIKELINESS;
		}
		else if (AnswerStyle.isAnswerSetMealStyle(answers))
		{
			//System.out.println("Found a meal style answer set");
			style = MEAL;
		}
		else if (AnswerStyle.isAnswerSetFamiliarityStyle(answers))
		{
			//System.out.println("Found a familiarity style answer set");
			style = FAMILIARITY;
		}
		else if (AnswerStyle.isAnswerSetTimeOfDayStyle(answers))
		{
			//System.out.println("Found a time of day style answer set");
			style = TIME_OF_DAY;
		}
		else if (AnswerStyle.isAnswerSetMotivationStyle(answers))
		{
			//System.out.println("Found a motivation style answer set");
			style = MOTIVATION;
		}
		else if (AnswerStyle.isAnswerSetYesNoStyle(answers))
		{
			//System.out.println("Found a yes/no style answer set");
			style = YES_NO;
		}
		else if (AnswerStyle.isAnswerSetCurrencyRangeStyle(answers))
		{
			//System.out.println("Found a currency range style answer set");
			style = CURRENCY_RANGE;
		}
		else if (AnswerStyle.isAnswerSetFrequencyStyle(answers))
		{
			//System.out.println("Found a frequency style answer set");
			style = FREQUENCY;
		}
		else if (AnswerStyle.isAnswerSetLikedDislikedStyle(answers))
		{
			//System.out.println("Found a liked/disliked style answer set");
			style = LIKED_DISLIKED;
		}
		else if (AnswerStyle.isAnswerSetCountStyle(answers))
		{
			style = COUNT;
		}
		else if (AnswerStyle.doesAnswerSetContainOtherOption(answers))
		{
			//System.out.println("Found a general answer set with other option");
			style = GENERAL_WITH_OTHER;
		}
		else
		{
			//System.out.println("General answer set");
			style = GENERAL;
		}
		
		for (Answer answer : answers)
		{
			answer.style = style;
		}
	}
}
