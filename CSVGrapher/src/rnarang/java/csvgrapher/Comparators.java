package rnarang.java.csvgrapher;

import java.util.ArrayList;
import java.util.Comparator;


public class Comparators 
{	
	public static abstract class AnswerStyleComparator implements Comparator<Answer>
	{
		protected abstract int getOrderIntForAnswer(Answer answer);
		
		public int compare(Answer o1, Answer o2) 
		{
			int a1 = getOrderIntForAnswer(o1);
			int a2 = getOrderIntForAnswer(o2);
			
			if (a1 > a2)
			{
				return -1;
			}
			else if (a1 < a2)
			{
				return 1;
			}
			else
			{
				if (o1.count > o2.count)
				{
					return -1;
				}
				else if (o1.count < o2.count)
				{
					return 1;
				}
				
				return 0;
			}
		}
	}
	
	public static class CountAnswerComparator extends AnswerStyleComparator
	{
		@Override
		protected int getOrderIntForAnswer(Answer answer) 
		{
			String a = answer.content.toLowerCase();
			
			if (a.contains("more than"))
			{
				return Integer.MAX_VALUE;
			}
			else if (a.contains("less than"))
			{
				return Integer.MIN_VALUE;
			}
			else
			{
				return Integer.parseInt(a);
			}			
		}
	}
	
	public static class LikedDislikedAnswerComparator extends AnswerStyleComparator
	{

		@Override
		protected int getOrderIntForAnswer(Answer answer) 
		{
			String a = answer.content.toLowerCase();
			
			if (a.equals("liked a lot"))
			{
				return 5;
			}
			else if (a.equals("liked somewhat"))
			{
				return 4;
			}
			else if (a.equals("neutral"))
			{
				return 3;
			}
			else if (a.equals("disliked somewhat"))
			{
				return 2;
			}
			else if (a.equals("disliked a lot"))
			{
				return 1;
			}
			
			return 0;
		}
		
	}
	
	public static class FrequencyAnswerComparator extends AnswerStyleComparator
	{

		@Override
		protected int getOrderIntForAnswer(Answer answer)
		{
			String a = answer.content.toLowerCase();
			
			if (a.equals("always"))
			{
				return 5;
			}
			else if (a.equals("often"))
			{
				return 4;
			}
			else if (a.equals("sometimes"))
			{
				return 3;
			}
			else if (a.equals("rarely"))
			{
				return 2;
			}
			else if (a.equals("never"))
			{
				return 1;
			}
			return 0;
		}
		
	}
	
	public static class RangeAnswerComparator extends AnswerStyleComparator
	{
		private ArrayList<Range> ranges;
		private ArrayList<Answer> answers;
		private boolean isAscending;
		
		@SuppressWarnings("unchecked")
		public RangeAnswerComparator(ArrayList<Range> ranges, ArrayList<Answer> answers) 
		{
			this.ranges = (ArrayList<Range>) ranges.clone();
			this.answers = (ArrayList<Answer>) answers.clone();
			isAscending = false;
		}
		
		@SuppressWarnings("unchecked")
		public RangeAnswerComparator(ArrayList<Range> ranges, ArrayList<Answer> answers, boolean ascending) 
		{
			this.ranges = (ArrayList<Range>) ranges.clone();
			this.answers = (ArrayList<Answer>) answers.clone();
			isAscending = ascending;
		}

		
		@Override
		protected int getOrderIntForAnswer(Answer answer) 
		{
			return 0;
		}
		
		@Override
		public int compare(Answer o1, Answer o2) 
		{
			int i1 = answers.indexOf(o1);
			int i2 = answers.indexOf(o2);
			
			Range r1 = ranges.get(i1);
			Range r2 = ranges.get(i2);
			
			if (r1.isGreaterThan(r2))
			{
				return isAscending ? 1 : -1;
			}
			else if (r1.isLesserThan(r2))
			{
				return isAscending ? -1 : 1;
			}
			else
			{
				return 0;
			}
		}
		
	}
	
	public static class YesNoAnswerComparator extends AnswerStyleComparator
	{

		@Override
		protected int getOrderIntForAnswer(Answer answer) 
		{
			String a = answer.content.toLowerCase();
			
			// The order below matters
			if (a.equals("yes"))
			{
				return 4;
			}
			else if (a.startsWith("yes"))
			{
				return 5;
			}
			else if (a.equals("no"))
			{
				return 3;
			}
			else if (a.startsWith("no ") || a.startsWith("no,"))
			{
				return 2;
			}
			else if (a.equals("unsure") || a.equals("not sure"))
			{
				return 1;
			}
			else if (a.equals("n/a"))
			{
				return 0;
			}
			
			return -1;
		}
		
	}
	
	public static class MotivationAnswerComparator extends AnswerStyleComparator
	{

		@Override
		protected int getOrderIntForAnswer(Answer answer) 
		{
			String motivation = answer.content.toLowerCase();
			
			// The order matters
			
			if (motivation.contains("not very"))
			{
				return 1;
			}
			else if (motivation.contains("somewhat"))
			{
				return 2;
			}
			else if (motivation.contains("moderately"))
			{
				return 3;
			}
			else if (motivation.contains("very"))
			{
				return 5;
			}
			else if (motivation.contains("motivated"))
			{
				return 4;
			}
			
			return 0;
		}
		
	}
	
	public static class TimeOfDayAnswerComparator extends AnswerStyleComparator
	{

		@Override
		protected int getOrderIntForAnswer(Answer answer) 
		{
			String a = answer.content.toLowerCase();
			
			if (a.contains("morning"))
			{
				return 5;
			}
			else if (a.contains("afternoon"))
			{
				return 4;
			}
			else if (a.contains("evening"))
			{
				return 3;
			}
			else if (a.contains("late night"))
			{
				return 2;
			}
			
			return 0;
		}
		
	}
	
	public static class FamiliarityAnswerComparator extends AnswerStyleComparator
	{
		@Override
		protected int getOrderIntForAnswer(Answer answer) 
		{
			String a = answer.content.toLowerCase();
			
			if (a.contains("not"))
			{
				return 2;
			}
			else if (a.contains("mostly"))
			{
				return 4;
			}
			else if (a.contains("somewhat"))
			{
				return 3;
			}
			else if (a.contains("very"))
			{
				return 5;
			}
			
			return 0;
		}
	}
	
	public static class MealAnswerComparator extends AnswerStyleComparator
	{
		@Override
		protected int getOrderIntForAnswer(Answer answer)
		{
			String m = answer.content.toLowerCase();
			
			if (m.equals("breakfast"))
			{
				return 5;
			}
			
			if (m.equals("lunch"))
			{
				return 4;
			}
			
			if (m.equals("snack"))
			{
				return 3;
			}
			
			if (m.equals("dinner"))
			{
				return 2;
			}
			
			if (m.equals("dessert"))
			{
				return 1;
			}
			
			if (m.equalsIgnoreCase("after exercising"))
			{
				return 0;
			}
			
			return 0;
		}
	}
	
	public static class LikelinessAnswerComparator extends AnswerStyleComparator
	{
		@Override
		protected int getOrderIntForAnswer(Answer answer)
		{
			String likeliness = answer.content.toLowerCase();
			String start = likeliness.substring(0, 1);
			
			try
			{
				return Integer.parseInt(start);
			}
			catch(NumberFormatException e)
			{
				if (likeliness.equals("very likely")) return 5;
				if (likeliness.equals("somewhat likely")) return 4;
				if (likeliness.equals("somewhat unlikely")) return 3;
				if (likeliness.equals("very unlikely")) return 2;
				else return 1;
			}
		}		
	}
	
	public static class RatingAnswerComparator extends AnswerStyleComparator
	{
		@Override
		protected int getOrderIntForAnswer(Answer answer)
		{			
			String rating = answer.content;
			
			if (rating.startsWith("10"))
			{
				return 10;
			}
			else if (rating.startsWith("1"))
			{
				return 1;
			}
			else if (rating.startsWith("5"))
			{
				return 5;
			}
			else
			{
				return Integer.parseInt(rating);
			}
		}
	}
	
	public static class GeneralAnswerWithOtherOptionComparator extends AnswerStyleComparator
	{

		@Override
		protected int getOrderIntForAnswer(Answer answer) 
		{
			String a = answer.content.toLowerCase(); 
			
			if (a.startsWith("other ") || a.startsWith("others ") || a.equals("other") || a.equals("others"))
			{
				return -1;
			}
			else
			{
				return answer.count;
			}
		}
		
	}
	
	public static class GeneralAnswerComparator extends AnswerStyleComparator
	{		
		@Override
		protected int getOrderIntForAnswer(Answer answer) 
		{
			return answer.count;
		}
	}
	
	public static AnswerStyleComparator getComparatorForAnswerSet(ArrayList<Answer> answers)
	{
		if (answers.isEmpty()) return null;
		
		int style = answers.get(0).style;
		
		switch(style)
		{
			case AnswerStyle.RATING:
			{
				return new Comparators.RatingAnswerComparator();
			}
			
			case AnswerStyle.LIKELINESS:
			{
				return new Comparators.LikelinessAnswerComparator();
			}
			
			case AnswerStyle.MEAL:
			{
				return new Comparators.MealAnswerComparator();
			}
			
			case AnswerStyle.FAMILIARITY:
			{
				return new Comparators.FamiliarityAnswerComparator();
			}
			
			case AnswerStyle.TIME_OF_DAY:
			{
				return new Comparators.TimeOfDayAnswerComparator();
			}
			
			case AnswerStyle.MOTIVATION:
			{
				return new Comparators.MotivationAnswerComparator();
			}
			
			case AnswerStyle.YES_NO:
			{
				return new Comparators.YesNoAnswerComparator();
			}
			
			case AnswerStyle.CURRENCY_RANGE:
			{
				ArrayList<Range> ranges = AnswerStyle.getRangeListForCurrencyRangeStyleAnswerSet(answers);
				return new Comparators.RangeAnswerComparator(ranges, answers);
			}
			
			case AnswerStyle.NUMERIC_RANGE:
			{
				ArrayList<Range> ranges = AnswerStyle.getRangeListForNumericRangeStyleAnswerSet(answers);
				return new Comparators.RangeAnswerComparator(ranges, answers, true);
			}
			
			case AnswerStyle.FREQUENCY:
			{
				return new Comparators.FrequencyAnswerComparator();
			}
			
			case AnswerStyle.LIKED_DISLIKED:
			{
				return new Comparators.LikedDislikedAnswerComparator();
			}
			
			case AnswerStyle.GENERAL_WITH_OTHER:
			{
				return new Comparators.GeneralAnswerWithOtherOptionComparator();
			}
			
			case AnswerStyle.COUNT:
			{
				return new Comparators.CountAnswerComparator();
			}
			
			default:
			{
				return new Comparators.GeneralAnswerComparator();
			}
		}
	}
}
