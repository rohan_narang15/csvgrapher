package rnarang.java.csvgrapher;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.TextAnchor;

import com.opencsv.CSVReader;

public class Main 
{	
	public static class PercentageCategoryItemLabelGenerator implements CategoryItemLabelGenerator
	{
		

		@Override
		public String generateColumnLabel(CategoryDataset dataset, int index) 
		{			
			return dataset.getColumnKey(index).toString();
		}

		@Override
		public String generateLabel(CategoryDataset dataset, int row, int column)
		{
			Number number = dataset.getValue(row, column);
			return String.format("%.2f", number.doubleValue()) + " %";
		}

		@Override
		public String generateRowLabel(CategoryDataset arg0, int arg1) 
		{
			return null;
		}
		
	}
	
	// Constants;	
	public static int SORT_ORDER_ASCENDING = 0;
	public static int SORT_ORDER_DESCENDING = 1;
	
	private static int sampleSize = 0;
	private static ArrayList<String> questions = new ArrayList<String>();
	private static ArrayList<ArrayList<Answer>> answerSets = new ArrayList<ArrayList<Answer>>();
	
	// Arguments
	private static String inputPath;
	private static int fontSize = 12;
	private static int questionNumber = -1;
	private static boolean shouldFillInMissingOptions = true;
	private static boolean shouldSortOptions = true;
	private static int sortOrder = SORT_ORDER_DESCENDING;
	
	public static void clearData()
	{
		sampleSize = 0;
		questions = new ArrayList<String>();
		answerSets = new ArrayList<ArrayList<Answer>>();
	}
	
	public static boolean doesListContainAnswerString(ArrayList<Answer> answers, String answerContent)
	{
		for (Answer answer : answers)
		{
			if (answer.content.equals(answerContent))
			{
				return true;
			}
		}
		return false;
	}
	
	public static void readFile(String filename)
	{
		try
		{
			FileReader fileReader = new FileReader(filename);
			CSVReader csvReader = new CSVReader(fileReader, ',', '"', '\0'); // Use comma as separator, quote as quotation, no escape characters.
			
			List<String[]> lines = csvReader.readAll();
			Iterator<String[]> iterator = lines.iterator();
			
			sampleSize = lines.size() - 1;
			
			if (iterator.hasNext())
			{
				String[] header = iterator.next();
				for (int i = 2; i < header.length - 1; ++i)
				{
					questions.add(header[i]);
					answerSets.add(new ArrayList<Answer>());
				}
			}
			
			while (iterator.hasNext())
			{
				String[] line = iterator.next();
				
				for (int i = 2; i < line.length - 1; ++i)
				{
					String answer = line[i];
					ArrayList<Answer> allAnswers = answerSets.get(i - 2);
					
					if (doesListContainAnswerString(allAnswers, answer) == false &&
						answer.trim().equals("") == false)
					{
						Answer a = new Answer();
						// Fix to remove corrupted apostrophes
						a.content = answer.replace("â€™","'");
						//a.content = answer;
						a.count = 1;						
						allAnswers.add(a);
					}
					else
					{
						Answer a = null;
						for (int j = 0; j < allAnswers.size(); ++j)
						{
							if (allAnswers.get(j).content.equals(answer))
							{
								a = allAnswers.get(j);
							}
						}
						
						if (a != null)
						{
							a.count = a.count + 1;							
						}
					}					
				}
			}
			
			csvReader.close();
			fileReader.close();
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println(e.getMessage());
			System.exit(1);
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}
	
	public static void dumpData()
	{
		for (int i = 0; i < questions.size(); ++i)
		{
			String question = questions.get(i);
			System.out.println((i + 1) + ") " + question);
			
			ArrayList<Answer> allAnswers = answerSets.get(i);

			for (int j = 0; j < allAnswers.size(); ++j)
			{
				Answer answer = allAnswers.get(j);
				System.out.println(answer.content + " Count: " + answer.count);
			}
		}
	}
	
	private static float clamp(float value, float lower, float upper)
	{
		if (value < lower) return value;
		if (value > upper) return value;
		return value;
	}
		
	private static JFreeChart createBarChart(ArrayList<Answer> allAnswers)
	{
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		double max = 0.0;
		for (int j = 0; j < allAnswers.size(); ++j)
		{
			Answer answer = allAnswers.get(j);
			
			double percentage = (answer.count / (double) sampleSize) * 100.0;				
			max = percentage > max ? percentage : max;
			dataset.setValue(percentage, "Percentage", answer.content);
		}
		
		PlotOrientation orientation = PlotOrientation.HORIZONTAL;
								
		JFreeChart barChart = ChartFactory.createBarChart("", "", "", dataset, orientation, false, false, false);
		barChart.setBackgroundPaint(Color.WHITE);
		barChart.setAntiAlias(true);
		barChart.setTextAntiAlias(true);
		
		CategoryPlot categoryPlot = barChart.getCategoryPlot();
		categoryPlot.setBackgroundPaint(Color.WHITE);
		categoryPlot.setDomainGridlinePaint(Color.WHITE);
		categoryPlot.setRangeGridlinePaint(Color.WHITE);
		categoryPlot.setOutlineVisible(false);
		categoryPlot.getRangeAxis().setVisible(false);
		categoryPlot.getRangeAxis().setAutoRange(false);
		categoryPlot.getRangeAxis().setRange(0.0, max + fontSize);
		
		categoryPlot.getDomainAxis().setMaximumCategoryLabelLines(3);
		categoryPlot.getDomainAxis().setCategoryLabelPositionOffset(10);
		categoryPlot.getDomainAxis().setTickLabelFont(new Font("Calibri", Font.PLAIN, fontSize));
		categoryPlot.getDomainAxis().setMaximumCategoryLabelWidthRatio(0.35f);
		
		BarRenderer renderer = new BarRenderer();
		renderer.setGradientPaintTransformer(null);
		renderer.setBarPainter(new StandardBarPainter());
		renderer.setShadowVisible(false);
		renderer.setBaseItemLabelGenerator(new PercentageCategoryItemLabelGenerator());
		renderer.setBaseItemLabelFont(new Font("Calibri", Font.PLAIN, fontSize));
		renderer.setBaseItemLabelsVisible(true);
		renderer.setBase(0.0);
		
		if (orientation == PlotOrientation.HORIZONTAL)
		{
			// Spacing between bar and the % numeral
			
			double spacing = 
					25.0f + // constant component 
					2.0f * clamp(fontSize - 8, 0, 25) + // Component that affects sizes > 8 
					10 * clamp(fontSize - 17, 0, 25); // Component that affects sizes > 17
			renderer.setItemLabelAnchorOffset(spacing);
			renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE3, TextAnchor.CENTER));
		}
		
		renderer.setSeriesPaint(0, Color.decode("0x991A1F"));
		
		categoryPlot.setRenderer(renderer);
		
		return barChart;
	}
	
	private static JFreeChart createPieChart(ArrayList<Answer> allAnswers)
	{
		DefaultPieDataset dataset = new DefaultPieDataset();
		
		double max = 0.0;
		for (int j = 0; j < allAnswers.size(); ++j)
		{
			Answer answer = allAnswers.get(j);
			
			double percentage = (answer.count / (double) sampleSize) * 100.0;				
			max = percentage > max ? percentage : max;
			
			dataset.setValue(String.format("%s %.2f", answer.content, percentage) + " %", percentage);
		}
										
		JFreeChart pieChart = ChartFactory.createPieChart("", dataset);
		pieChart.setBackgroundPaint(Color.WHITE);
		pieChart.setAntiAlias(true);
		pieChart.setTextAntiAlias(true);
		
		PiePlot plot = (PiePlot) pieChart.getPlot();
		plot.setBackgroundPaint(Color.WHITE);
		plot.setForegroundAlpha(1.0f);
		plot.setShadowPaint(Color.WHITE);
		plot.setOutlineVisible(false);
		plot.setLabelOutlinePaint(Color.WHITE);
		plot.setLabelBackgroundPaint(Color.WHITE);
		plot.setLabelShadowPaint(Color.WHITE);
		plot.setShadowPaint(Color.WHITE);
		plot.setLabelFont(new Font("Calibri", Font.PLAIN, fontSize));
		plot.setSectionOutlinesVisible(false);
		plot.setSectionPaint(0, Color.decode("0x991A1F"));
		plot.setSectionPaint(1, Color.decode("0x991A1F").brighter());
		plot.setSectionPaint(2, Color.decode("0x991A1F").brighter().brighter());
		
		pieChart.removeLegend();
		
		/*
		CategoryItemRenderer renderer = plot.getR
		renderer.setBaseItemLabelGenerator(new PercentageCategoryItemLabelGenerator());
		renderer.setBaseItemLabelFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		renderer.setBaseItemLabelsVisible(true);
				
		renderer.setSeriesPaint(0, Color.decode("0x991A1F"));
		*/
		
		return pieChart;

	}
	
	public static void createImageAtPath(int questionNumber, String path, String name)
	{
		File dir = new File (path + "/" + name);
		dir.mkdirs();
		
		System.out.println("Created Directory: " + dir.getAbsolutePath());
		
		int i = questionNumber - 1;
		
		ArrayList<Answer> allAnswers = answerSets.get(i);

		System.out.println("Processing Question: " + questions.get(i));

		if (allAnswers.isEmpty())
		{
			System.out.println("No answers found for current question");
			return;
		}

		JFreeChart chart = null;
		int width;
		int height;

		if (allAnswers.size() > 3)
		{
			width = 800;
			height = 400;

			// Find the maximum length answer and save its length in max
			int max = 0;
			for (Answer answer : allAnswers)
			{
				max = Math.max(answer.content.length(), max);
			}

			// Calculate a threshold length value based on the font size
			int thresholdLength = 100 - fontSize * 2;

			// If there are more than 10 answers of if the max length of any answer is greater than the threshold
			// calculate a custom size
			if (allAnswers.size() > 10 || max >= thresholdLength)
			{
				// Custom size formula
				height = 400 + allAnswers.size() * 35;
			}

			chart = createBarChart(allAnswers);
		}
		else
		{
			width = 700;
			height = 500;
			chart = createPieChart(allAnswers);
		}

		// Sanitize the question string for creating an image filename string
		String imageName = questions.get(i).
				replace(" ", "_").
				replace("\\", "_").
				replace("/", "_").
				replace("?", "_").
				replace("\"", "_").
				replace("<", "_").
				replace(">", "_").
				replace("*", "_").
				replace(":", "_").
				replace("|", "_");

		String filename = path + "/" + name + "/" + imageName + ".png";

		try 
		{
			ChartUtilities.saveChartAsPNG(new File(filename), chart, width, height);
			System.out.println("Exported PNG for current question: " + imageName);
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}
	
	public static void createImagesAtPath(String path, String name)
	{
		File dir = new File (path + "/" + name);
		dir.mkdirs();
		
		System.out.println("Created Directory: " + dir.getAbsolutePath());
		
		for (int i = 0; i < questions.size(); ++i)
		{
			ArrayList<Answer> allAnswers = answerSets.get(i);
			
			System.out.println("Processing Question: " + questions.get(i));
			
			if (allAnswers.isEmpty())
			{
				System.out.println("No answers found for current question. Continuing...");
				continue;
			}
						
			JFreeChart chart = null;
			int width;
			int height;

			if (allAnswers.size() > 3)
			{
				width = 800;
				height = 400;
				
				// Find the maximum length answer and save its length in max
				int max = 0;
				for (Answer answer : allAnswers)
				{
					max = Math.max(answer.content.length(), max);
				}
				
				// Calculate a threshold length value based on the font size
				int thresholdLength = 100 - fontSize * 2;
				
				// If there are more than 10 answers of if the max length of any answer is greater than the threshold
				// calculate a custom size
				if (allAnswers.size() > 10 || max >= thresholdLength)
				{
					// Custom size formula
					height = 400 + allAnswers.size() * 35;
				}
				
				chart = createBarChart(allAnswers);
			}
			else
			{
				width = 700;
				height = 500;
				chart = createPieChart(allAnswers);
			}
			
			// Sanitize the question string for creating an image filename string
			String imageName = questions.get(i).
					replace(" ", "_").
					replace("\\", "_").
					replace("/", "_").
					replace("?", "_").
					replace("\"", "_").
					replace("<", "_").
					replace(">", "_").
					replace("*", "_").
					replace(":", "_").
					replace("|", "_");
			
			String filename = path + "/" + name + "/" + imageName + ".png";
			
			try 
			{
				ChartUtilities.saveChartAsPNG(new File(filename), chart, width, height);
				System.out.println("Exported PNG for current question: " + imageName);
			} 
			catch (IOException e) 
			{
				System.out.println(e.getMessage());
				System.exit(1);
			}
		}
	}
	
	public static void processAnswerSets()
	{		
		for (ArrayList<Answer> answerSet : answerSets)
		{
			// Detect Answer style for set
			AnswerStyle.detectAnswerStyleForSet(answerSet);
			
			// Fill In Missing answers if any			
			if (shouldFillInMissingOptions)
			{
				AnswerStyle.fillInMissingAnswersForSet(answerSet);
			}
			
			// Sort the answer set
			if (shouldSortOptions)
			{
				if (sortOrder == SORT_ORDER_DESCENDING)
				{
					Comparators.AnswerStyleComparator comparator = Comparators.getComparatorForAnswerSet(answerSet);
					answerSet.sort(comparator);
				}
				else
				{
					Comparators.AnswerStyleComparator comparator = Comparators.getComparatorForAnswerSet(answerSet);
					answerSet.sort(comparator);
					Collections.reverse(answerSet);
				}
			}			
		}
	}
	
	public static void processCsv(String path)
	{
		File file = new File(path);
		String csvName = file.getName();
		String pngName = csvName.substring(0, csvName.indexOf("."));
		
		//System.out.println("Processing File: " + csvName);
		
		readFile(path);
		processAnswerSets();
		
		if (questionNumber == -1)
		{
			createImagesAtPath(file.getParent(), pngName);
		}
		else
		{
			createImageAtPath(questionNumber, file.getParent(), pngName);
		}
		
		clearData();
	}
	
	public static boolean isFilenameCsv(String filename)
	{
		return filename.substring(filename.indexOf(".") + 1, filename.length()).equals("csv");
	}
	
	public static void parseArguments(String[] args)
	{
		
		if (args.length == 0 || args.length % 2 != 0)
		{
			System.out.println("Invalid Usage.");
			displayUsage();
			System.exit(0);
		}
		
		for (int i = 0; i < args.length; i = i + 2)
		{
			String key = args[i];
			String value = args[i + 1];
			
			System.out.println("Key: " + key);
			System.out.println("Value: " + value);
			
			if (key.equals("-font_size"))
			{
				try
				{
					fontSize = Integer.parseInt(value);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Error: Invalid value for option 'font size'");
					System.exit(0);
				}
			}
			else if (key.equals("-fill_missing_numeric_options"))
			{
				if (value.equals("true"))
				{
					shouldFillInMissingOptions = true;
				}
				else if (value.equals("false"))
				{
					shouldFillInMissingOptions = false;
				}
				else
				{
					System.out.println("Error: Invalid value for option 'dont fill missing numeric options'");
					System.exit(0);
				}
			}
			else if (key.equals("-ordering"))
			{
				
				if (value.equals("none"))
				{
					shouldSortOptions = false;
				}
				else if (value.equals("ascending"))
				{
					shouldSortOptions = true;
					sortOrder = SORT_ORDER_ASCENDING;
				}
				else if (value.equals("descending"))
				{
					shouldSortOptions = true;
					sortOrder = SORT_ORDER_DESCENDING;
				}
				else
				{
					System.out.println("Error: Invalid value for option 'ordering'");
					System.exit(0);
				}
			}
			else if (key.equals("-input"))
			{
				File file = null;
				
				try
				{
					file = new File(value);
					inputPath = value;
				}
				catch(NullPointerException e)
				{
					System.out.println("Invalid path. Please input a valid csv file path or a folder with csv files");
					System.exit(0);
				}
			}
			else if (key.equals("-question_number"))
			{
				try
				{
					questionNumber = Integer.parseInt(value);
				}
				catch (NumberFormatException e)
				{
					System.out.println("Error: Invalid value for option 'question number'");
					System.exit(0);
				}
			}
			else
			{
				System.out.println("Invalid option: " + key + ". Please check the usage for a list of valid options");
				System.exit(0);
			}			
		}
		
		if (inputPath == null || inputPath == "")
		{
			System.out.println("Error: No input found");
			System.exit(0);
		}		
	}
	
	public static void displayUsage()
	{
		System.out.println("CSVGrapher Usage: ");
		System.out.println("CSVGrapher <option> <value>");
		System.out.println("Options: Required Value");
		System.out.println("-input: A path to the csv file or a folder containing csv files");
		System.out.println("-question_number: An integer signifying the question number to create image for");
		System.out.println("-font_size: An integer greater than 0");
		System.out.println("-fill_missing_numeric_options: Fills the missing numeric options for question numbers in value. Boolean");
		System.out.println("-ordering: Orders the answers. 'ascending', 'descending' or 'none'");
	}
	
	public static void main(String[] args) 
	{		
		// Parse args
		parseArguments(args);
						
		
		File file = null;
		// Create a file object
		try
		{
			file = new File(inputPath);
		}
		catch(NullPointerException e)
		{
			System.exit(0);
		}
		
		int extensionIndex = file.getName().indexOf(".");
		
		// If file is a directory search for csv files and process all of them
		if (extensionIndex == -1 && file.isDirectory())
		{
			for (File innerFile : file.listFiles()) 
			{
				if (innerFile.isFile() && isFilenameCsv(innerFile.getName()))
				{
					processCsv(innerFile.getAbsolutePath());
				}
			}
			
			System.out.println("Done");
		}
		else
		{
			String filename = file.getName();
			
			// Check for csv format extension before processing
			if (isFilenameCsv(filename))
			{
				// Process the single file
				processCsv(inputPath);
				System.out.println("Done");
			}
			else
			{
				System.out.println("Please input a valid csv file path or a folder with csv files");
				System.exit(0);
			}
		}
	}
}
