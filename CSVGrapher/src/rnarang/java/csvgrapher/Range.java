package rnarang.java.csvgrapher;

public class Range 
{
	private double upperLimit;
	private double lowerLimit;
	
	public Range(double ul, double ll)
	{
		upperLimit = ul;
		lowerLimit = ll;
	}
	
	public double getLowerLimit()
	{
		return lowerLimit;
	}
	
	public double getUpperLimit()
	{
		return upperLimit;
	}
	
	public boolean isGreaterThan(Range range)
	{
		return this.lowerLimit >= range.upperLimit;
	}
	
	public boolean isLesserThan(Range range)
	{
		return this.upperLimit <= range.lowerLimit;
	}
}
